#!/bin/env python3

"""
A program to copy and rename a KiCad repository.

Usage: kicad_rename.py [-h] <repository> <new_repository>

Where <repository>     is the name of the repository to copy
      <new_repository> is the name of the new repository to create
"""

import os
import sys
import getopt
import traceback
import logger
import subprocess

log = logger.Log('kicad_rename.log', logger.Log.DEBUG)


def abort(msg):
    """Log and print error message "msg"."""

    log(msg)
    print(msg)

def usage(msg=None):
    """Help the befuddled user."""

    if msg:
        print(('*'*80 + '\n%s\n' + '*'*80) % msg)
    print(__doc__)

def excepthook(type, value, tb):
    """Our own handler for uncaught exceptions"""

    msg = '\n' + '=' * 80
    msg += '\nUncaught exception:\n'
    msg += ''.join(traceback.format_exception(type, value, tb))
    msg += '=' * 80 + '\n'
    log(msg)
    print(msg)

def file_replace(filename, old, new):
    """Replace string "old" with "new" in file "filename"."""

    print(f'file_replace: {filename}')

    with open(filename, 'r') as fd:
        lines = fd.readlines()

    with open(filename, 'w') as fd:
        for l in lines:
            l = l.replace(old, new)
            fd.write(l)

def kicad_rename(old, new):
    """Copy and rename kicad "old" to "new"."""

    print(f'kicad_rename() called, old={old}, new={new}')

    # check that the "old" directory is there.
    if not os.path.isdir(old):
        abort(f"Old directory '{old}' doesn't exist.")
        return 1

    # make sure not overwriting output directory
    if os.path.isdir(new):
        abort(f"New directory '{old}' exists, won't overwrite.")
        return 1

    # create the "new" directory by copying
    try:
        copy_cmd = ['cp', '-R', old, new]
        log("Doing: '{cmd}'")
        result = subprocess.check_output(copy_cmd).decode("utf-8")
    except subprocess.CalledProcessError as e:
        log.critical("Error doing: '{cmd}'")
        print("Error copying KiCad directory")
        abort(f'Error status {e.returncode}')

    # first, get all files/dirs with "old" in the name and rename them
    file_list = []
    for root, dirs, files in os.walk(new):
        for f in files+dirs:
            if f.startswith('.'):
                continue
            if old in f:
                new_f = f.replace(old, new)
                file_list.append((os.path.join(root, f), os.path.join(root, new_f)))

    for (f, new_f) in file_list:
        os.rename(f, new_f)

    # second, find all files containing "old" and change to "new"
    file_list = []
    for root, dirs, files in os.walk(new):
        for f in files:
            if f.startswith('.'):
                continue
            file_list.append((os.path.join(root, f)))

    for f in file_list:
        file_replace(f, old, new)

    return 0

def main():
    """Run the system."""

    # plug our handler into the python system
    sys.excepthook = excepthook

    # parse the CLI params
    argv = sys.argv[1:]

    try:
        (opts, args) = getopt.getopt(argv, 'h', ['help'])
    except getopt.GetoptError as err:
        usage(err)
        sys.exit(1)

    for (opt, param) in opts:
        if opt in ['-h', '--help']:
            usage()
            sys.exit(0)

    # check that we have the two directories
    if len(args) != 2:
        usage()
        sys.exit(1)

    # run the program code
    result = kicad_rename(*args)
    sys.exit(result)

main()
