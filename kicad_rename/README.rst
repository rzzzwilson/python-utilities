kicad_rename
============

A utility to "rename" a KiCad repository.  Something like this is needed
because it's impossible to "fork" an existing repository - any changes
lose the original design.

Usage
-----

Usage:  kicad_rename.py <repository> <new_repository>

This will create a new repository *<new_repository>* with all files in
the new repository having all references to *<repository>* renamed to
*<new_repository>*.

This command will create a new directory containing the old project with 
all fiules renamed and any reference to the old project name changed to
the new name.

What's here
-----------

The *PWKeep* directory is an example directory that is to be renamed.  Note
that this project is in the wrong place so will not work correctly as a
KiCad project.

The *XYZZY* directory will be created by `make`.  This is a renamed version of
*PWKeep* and should work in-place if the rename process works successfully.

The *Makefile* has two target: *clean* to remove temporary stuff, and *test*
to rename *PWKeep* to *XYZZY*.

*kicad_rename.py* is the program to rename KiCad projects, and *logger.py*
is a utility logging module.

Status
------

The code to make the obvious changes to filenames and internal text where
necessary is working.

The less obvious changes to absolute paths have yet to be done.
