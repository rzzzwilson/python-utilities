1
00:02:15,083 --> 00:02:16,833
When shall we three meet again?

2
00:02:17,917 --> 00:02:20,708
In thunder, lightning, or in rain?

3
00:02:21,500 --> 00:02:23,375
When the hurly-burly's done.

4
00:02:23,875 --> 00:02:25,583
When the battle's lost and won.

5
00:02:26,292 --> 00:02:27,458
Where the place?
