srt_move
========

A program to move timings in an SRT file.

Do this to see the optiona available:

::

    $ fdump -h

    A script to move the timing of an SRT file forward or backward.
    
    Usage:
    
        srt_move [-h] <srt_file> (+|minus) <time>
    
    where <srt_file> is an SRT filename
          <time>     is a time in seconds
    
    The <srt_file> file is rewritten with all times +/- <time>.
    
    Currently doesn't overwrite the input file but writes to "test.out".
