print2pdf
=========

Since my laser printer is no more I'm reduced again to taking files to print
to the nearest print shop.  Trouble is, I'm used to the convenience of my *print*
utility that produces nice 2-up code listings.

This program will take a text file and produce PDFs that will contain pages in
the style of *print*.

usage
-----

::

    print2pdf [-f] [-h] [-n] [-o <output file>] <file to print>

    where -f                shows FULL path to file in title bar,
          -h                prints help text and stops,
          -n                suppresses line numbers,
          -o <output file>  sets the output filename
                            if not specified, the name of the output PDF is
                            produced from the <file to print> name and is
                            created in the current directory.


The file *print2pdf.pdf* shows the result of processing the source code.
