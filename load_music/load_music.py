#!/usr/bin/env python

"""
Try to automate downloading with "pytubefix".

Usage: load_music.py [-a] [-d <output_dir>] [-f] <filename>

where <filename>    is a file with lines:
                      <url> <save_name>
      <output_dir>  is the output directory name
                    the default is "."
      -a            download only audio files
      -f            forces all files to be overwritten
      -p <seconds>  number of seconds between each download
                    (defaults to 25s)
"""

import sys
import time
import os.path
import getopt
try:
    from pytubefix import YouTube
    from pytubefix.cli import on_progress
except ModuleNotFoundError:
    print("*"*50)
    print(" pytubefix can't be imported, have you done 'workon pytubefix'?")
    print("*"*50)
    sys.exit(1)


log_file = "load_music.log"
sleep_time = 5


def log(msg):
    """Simple logging function."""

    with open(log_file, "a") as fd:
        fd.write(msg + "\n")

def load_video(url, output_path, filename, force):
    log(f"load_video: {url=}, {output_path=}, {filename=}, {force=}")

    yt = YouTube(url)
    
    ys = yt.streams.get_highest_resolution()
    ys.download(output_path=output_path, filename=filename, skip_existing=not force)

def load_audio(url, output_path, filename, force):
    log(f"load_audio: {url=}, {output_path=}, {filename=}, {force=}")
 
    yt = YouTube(url)
    
    ys = yt.streams.get_audio_only()
    ys.download(output_path=output_path, filename=filename, skip_existing=not force)


def main(filename, directory, forced, audio_only, pause):
    """Save all YT videos in 'filename'.

    filename    the data file to read
    directory   the directory to save into
    forced      if True previously downloaded files will be overwritten
    audio_only  True if audio only
    pause       number of seconds between downloads
    """

    with open(filename) as fd:
        for line in fd:
            line = line.strip()
            if line.startswith("#"):
                continue

            line = line.split()
            if len(line) != 2:
                print(f"Bad file format, line={line}")
                return 1

            (url, name) = line

            if audio_only:
                load_audio(url, directory, f"{name}.m4a", forced)
            else:
                load_video(url, directory, f"{name}.mp4", forced)

            log(f"Finished, sleeping {sleep_time} seconds.")
            time.sleep(pause)

    return 0

def usage(msg=None):
    """Print the app documentation with optional message."""

    if msg:
        print("#" * 60)
        print(msg)
        print("#" * 60 + "\n")
    print(__doc__)

###########################################################

# parse the CLI params
argv = sys.argv[1:]
try:
    (opts, args) = getopt.getopt(argv, "ad:fh", ["audio", "dir=", "force", "help"])
except getopt.GetoptError as err:
    usage(err)
    sys.exit(1)

# default the output directory to the current directory
output_dir = "."

# default to NOT overwriting files already downloaded
forced = False
audio_only = False
pause = sleep_time

# parse the options
for (opt, param) in opts:
    if opt in ["-a", "--audio"]:
        audio_only = True
    if opt in ["-d", "--dir"]:
        output_dir = param
    if opt in ["-f", "--force"]:
        forced = True
    elif opt in ["-h", "--help"]:
        usage()
        sys.exit(0)
    elif opt in ["-p", "--pause"]:
        try:
            pause = int(param)
        except ValueError:
            usage("Pause value must be an integer.")
            sys.exit(10)

if len(args) != 1:
    usage()
    sys.exit(1)
filename = args[0]

# empty the log file
with open(log_file, "w") as fd:
    pass

# kick it all off
sys.exit(main(filename, output_dir, forced, audio_only, pause))
