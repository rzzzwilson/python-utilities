#!/usr/bin/env python

"""
Download one Youtube file.

Usage: ytdl.py [-a] [-h] <URL> <outputfile>

where <URL>         is the Youtube URL to download
      <outputfile>  is the file to save the URL data to
                    (file extension is automatically added)
      -a            download only the audio of the URL data
      -h            prints this help and then stops
"""

import sys
import os.path
import getopt
try:
    from pytubefix import YouTube
except ModuleNotFoundError:
    print("Have you forgotten to do 'workon pytubefix'?")
    sys.exit(1)


def log(msg):
    """Simple logging function."""

    with open(log_file, "a") as fd:
        fd.write(msg + "\n")

def load_video(url, output_path):
    yt = YouTube(url)

    (output_dir, filename) = os.path.split(output_path)
    
    ys = yt.streams.get_highest_resolution()
    ys.download(output_path=output_dir, filename=filename)

def load_audio(url, output_path):
    yt = YouTube(url)
    
    (output_dir, filename) = os.path.split(output_path)
    
    ys = yt.streams.get_audio_only()
    ys.download(output_path=output_dir, filename=filename)


def main(url, filename, audio_only):
    """Save 'url' of YT video in 'filename'.

    url         the Youtube video
    filename    the data file to read
    audio_only  True if audio only
    """

    if audio_only:
        load_audio(url, f"{filename}.m4a")
    else:
        load_video(url, f"{filename}.mp4")

    return 0

def usage(msg=None):
    """Print the app documentation with optional message."""

    if msg:
        print("#" * 60)
        print(msg)
        print("#" * 60 + "\n")
    print(__doc__)

###########################################################

# parse the CLI params
argv = sys.argv[1:]
try:
    (opts, args) = getopt.getopt(argv, "ah", ["audio", "help"])
except getopt.GetoptError as err:
    usage(err)
    sys.exit(1)

# default the arguments
audio_only = False

# parse the options
for (opt, param) in opts:
    if opt in ["-a", "--audio"]:
        audio_only = True
    elif opt in ["-h", "--help"]:
        usage()
        sys.exit(0)

if len(args) != 2:
    usage()
    sys.exit(1)
url = args[0]
filename = args[1]

# empty the log file
#with open(log_file, "w") as fd:
#    pass

# kick it all off
sys.exit(main(url, filename, audio_only))
