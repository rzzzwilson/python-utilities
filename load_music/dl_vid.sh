#!/bin/env/bash

#
# Script to download all of the configured Audio/Video files
#
# Usage: dl_vid.sh
#

USB_DEVICE="/media/r-w/F91C-7D0C"

python ./load_music.py -d ${USB_DEVICE}/Video/ClassicalMusic url_data.txt
python ./load_music.py -d ${USB_DEVICE}/Audio/ClassicalMusic -a url_data.txt
