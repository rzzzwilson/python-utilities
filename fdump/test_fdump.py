import fdump

data_file = 'test.txt'

with open(data_file, 'rb') as fd:
    data = fd.read()

result = fdump.hexdump(data, 16, eightbit=True)
for x in result:
    print(x)
