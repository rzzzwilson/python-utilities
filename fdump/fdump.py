#!/usr/bin/env python3

"""File hexdump program.

usage: fdump [-V] [-h] [-8] [-l <linelength>] [-o <outfile>] <infile>

where <infile>     is the file to produce the hexdump of, and
      <linelength> the number of bytes dumped per line (default 16)
      <outfile>    is the output file of fdump, if supplied.

If the "-8" option is used then "printable" characters are those printable
characters in the full 8-bit set, not just the normal 7-bit ASCII printables.

If the "-V" option is specified then the utility version string is printed
and then the program stops.
"""

######
# This module is written so it may be used as a commandline utility as well
# as an importable module providing the "fdump.hexdump()" function:
#
#     import fdump
#     data = <byte data from somewhere>
#     hex_data = fdump.hex_dump(data, linelength=16, eightbit=False)
#     print(hex_data)
######

import math


# default number of bytes displayed per line
_NumBytesPerLine = 16

# define what we print if the character is unprintable
_UnprintableChar = '.'

# version string
Version = "1.0"


def _isprintable(ch, eightbit):
    """Returns True if byte value is printable.

    ch        the byte value to print
    eightbit  True if 8 bit chars are printable

    We don't allow U+00AD to be printable.  Doesn't work under MacOS.
    """

    if eightbit:
        return (ord(' ') <= ch <= ord('~') or 0xA1 <= ch <= 0XFF) and ch != 0x00AD

    # otherwise allow only 7 bit chars
    return ord(' ') <= ch <= ord('~')


def _hexdump_line(bseq, offset, linelength, owidth, eightbit):
    """Generate one line of fdump output.

    bseq        byte sequence to convert (linelength bytes or less)
    offset      offset from start of dump
    linelength  the number of bytes in one line
    owidth      the number of digits for the HEX offset number
    eightbit    True if allowing 8 bit printable chars
    """

    hexlist = []
    chlist = []
    for ch in bseq:
        hexlist.append(f'{ch:02x}')
        if _isprintable(ch, eightbit):
            chlist.append(chr(ch))
        else:
            chlist.append(_UnprintableChar)

    hexbuff = ' '.join(hexlist)
    chbuff = ''.join(chlist)

    hex_len = linelength * 3
    char_len = linelength

    hexbuff = (hexbuff + ' '*hex_len)[:hex_len]
    chbuff = (chbuff + '|' + ' '*char_len)[:char_len+1]

    return f'{offset:0{owidth}X}  {hexbuff} |{chbuff}  {offset}'


def hexdump(bseq, linelength=_NumBytesPerLine, eightbit=False):
    """Convert a sequence of bytes to a list of "hexdump" lines.

    bseq        the sequence of bytes to convert
    linelength  the number of bytes in one hexdump line
    eightbit    True if wanting printable 8 bit data

    Returns a list of strings containing "hexdump" information.
    """

    result = []
    offset = 0      # offset into "bseq" to get next "linelength" bytes

    owidth = math.ceil(math.log(len(bseq), 16))
    owidth = math.ceil(owidth/2) * 2

    while offset < len(bseq):
        sample = bseq[offset:offset+linelength]

        line = _hexdump_line(sample, offset, linelength, owidth, eightbit)
        result.append(line)
        offset += linelength

    return result

################################################################################

if __name__ == "__main__":
    import sys
    import os.path
    import getopt


    def hexdump_file(filename, linelength=_NumBytesPerLine, eightbit=False,
                     outfile=sys.stdout):
        """A function that generates a 'hex dump' of a file.

        filename    name of the file to dump
        linelength  number of bytes dumped per line
        eightbit    True if allowing 8 bit printable chars
        outfile     file-like object the hex data is written to

        Calling this function for a file containing
        'The quick brown fox\n\tjumps over the lazy dog.'
        returns three strings containing:
        0000  54 68 65 20 71 75 69 63 6b 20 62 72 6f 77 6e 20  |The quick brown |  0
        0010  66 6f 78 0a 09 6a 75 6d 70 73 20 6f 76 65 72 20  |fox..jumps over |  16
        0020  74 68 65 20 6c 61 7a 79 20 64 6f 67 2e           |the lazy dog.|     32

        +---------+---------+---------+---------+---------+---------+---------+-------
        0         1         2         3         4         5         6         7
        0         0         0         0         0         0         0         0

        Each line of the dump contains info about 16 bytes of the input string.

        The hex digits starting at column 0 are the offset in hex of the first byte.

        The 16 hex digit pairs starting at column 6 and ending at column 52 are the
        16 byte values in hex.

        The characters spanning columns 56-71 are the 16 printable characters.  If a
        character is not printable it is replaced by a '.' character.

        The decimal number starting at column 75 is the decimal offset of the first
        byte in the line.
        """

        # get the input filesize
        filesize = os.path.getsize(filename)
        if filesize == 0:
            print(f"File '{filename}' is empty.")
            return

        # compute the hex offset width required (multiple of 2)
        offset_width = math.ceil(math.log(filesize, 16))
        offset_width = math.ceil(offset_width/2) * 2

        # we used to read the entire file into memory but this delayed things
        # when doing "fdump really_big_file | more", so now read just enough for
        # a single line
        with open(filename, 'rb', 8192) as f:
            offset = 0
            while True:
                data = f.read(linelength)  # read up to 'linelength' bytes
                if len(data) == 0:
                    break

                try:
                    line = _hexdump_line(data, offset, linelength,
                                         offset_width, eightbit)
                    print(line, file=outfile)
                except IOError:
                    break

                offset += linelength

    def usage(msg=None):
        """Give the user some help."""

        if msg:
            print('-' * 60)
            print(msg)
            print('-' * 60, '\n')
        print(__doc__)

    def main(argv):
        """Get options from the command line and call the file dump code."""

        try:
            opts, args = getopt.gnu_getopt(argv, "hl:o:8V",
                                           ["help", "linelength=",
                                            "outfile=", "eight",
                                            "version"])
        except getopt.GetoptError:
            usage('Bad option')
            return 10

        linelength = _NumBytesPerLine   # bytes in one line
        eightbit= False                 # only 7 bit printables
        outfile = sys.stdout            # "stdout" is default output

        for opt, arg in opts:
            if opt in ("-V", "--version"):
                print(Version)
                return 0
            if opt in ("-8", "--eight"):
                eightbit = True
            if opt in ("-o", "--outfile"):
                try:
                    outfile = open(arg, 'w')
                except (PermissionError, FileNotFoundError,
                        IsADirectoryError) as e:
                    usage(f"You can't create output file: {e}")
                    return 1
            if opt in ("-l", "--linelength"):
                try:
                    linelength = int(arg)
                except ValueError:
                    usage('New line length must be an integer')
                    return 1
                if linelength < 1:
                    usage('New line length must be greater than zero')
                    return 1
            if opt in ("-h", "--help"):
                usage()
                return 0

        if len(args) != 2:
            usage()
            return 10

        # check input file actually exists
        filename = args[1]
        try:
            with open(filename) as f:
                pass
        except IOError:
            print(f"Sorry, can't find file '{filename}'.")
            return 10

        hexdump_file(filename, linelength=linelength,
                     eightbit=eightbit, outfile=outfile)

    try:
        sys.exit(main(sys.argv))
    except KeyboardInterrupt:
        # handle ^C nicely
        print()
