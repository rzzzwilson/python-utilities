from os import system
from os.path import isfile, splitext, join, basename

InputDir = "/media/r-w/F91C-7D0C/Video/ClassicalMusic/"
OutputDir = "Music"

onlyfiles = [f for f in os.listdir(InputDir) if isfile(join(InputDir, f))]
for f in onlyfiles:
    infile = join(InputDir, f)
    (stem, _) = splitext(f)
    outfile = join(OutputDir, stem + ".mp3")
    system(f"ffmpeg -i {infile} -q:a 0 -map a {outfile}")
