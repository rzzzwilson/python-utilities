This utility is to be used to find the latest "Ukraine: The Latest"
video from The Telegraph and download just the audio portion.

To use, just execute:

    ./yt_ukraine.py Audio

and select the files you want to play.

To remove data in the *\*.m4a* afterwards, execute:

    bash empty.sh

Eventually the aim is to have this running automatically on a
Raspberry Pi 3b+ and make the audio available on a web page.
