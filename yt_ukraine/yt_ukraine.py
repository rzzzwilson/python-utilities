#!/usr/bin/env python

"""
Get latest podcasts from The Telegraph, "Ukraine: The Latest".

Usage: yt-ukraine.py [-h] <directory>

where  -h           prints this help and then stops
       <directory>  is the directory to save audio files in
"""

import sys
import os.path
import getopt
from pprint import pprint
from bs4 import BeautifulSoup 
import requests 
from requests_html import HTMLSession
try:
    from pytubefix import YouTube
except ModuleNotFoundError:
    print("Unable to import 'pytubefix'.")
    print("Have you forgotten to do 'workon pytubefix'?")
    sys.exit(1)


# configuration
telegraph_videos = "https://www.youtube.com/c/telegraph/videos"
log_file = "yt_ukraine.log"

# Title string black list
# If any strings here are in the title, we don't want the video
black_list = ["Bed of Lies", "Planet Normal", "The Daily T", "Rugby Podcast"]

# Title string white list
# If any strings here are in the title, we do want the video
white_list = ["Ukraine: The Latest", "Ukraine", "Battle Lines", " | Dispatch",
              " | Podcast"]


def log(msg):
    """Write log messages to the log file."""

    with open(log_file, "a") as fd:
        fd.write(f"{msg}\n")

def load_audio(url, output_path):
    if os.path.exists(output_path):
        log(f"File {output_path} already exists, skipping")
        print(f"File {output_path} already exists, skipping")
        return

    yt = YouTube(url)
    
    (output_dir, filename) = os.path.split(output_path)
   
    for i in range(5):
        ys = yt.streams.get_audio_only()
        log(f"load_audio: attempt {i}, {url=}, {output_path=}")
        if ys:
            ys.download(output_path=output_dir, filename=filename)
            return

    log(f"Url {url} failed download!/")

def check_podcast(url): 
    """Returns date string if 'url' is Ukraine podcast.

    Returns None if not the desired podcast.
    """
   
    log(f"check_podcast: {url=}")

    r = requests.get(url) 
    s = BeautifulSoup(r.text, "html.parser") 
    date = s.find("meta", itemprop="datePublished")["content"]
    if date is None:
        log("ERROR: can't get date!?")
        print("ERROR: can't get date!?")
        return None
    log(f"text is '{s.text}'")

    for black in black_list:
        if black in s.text:
            log(f"{black} is in the text, ignore")
            return None

#    with open("html.out", "a") as fd:
#        fd.write(r.text)
#        fd.write("\n\n======================================================\n")

    for white in white_list:
        if white in s.text:
            return date

    return date
#    log("Not in whitelist, ignored")
#    return None

def save_podcast(url, date, save_dir):
    """Save audio of the URL into a file.

    The 'date' string is aded to the filename.
    Files are saved in the 'save_dir' directory.
    """

    video_id = url.split("=")[-1]
    filename = os.path.join(save_dir, f"Podcast_{date}_{video_id}")
    log(f"Saving audio from {url} into file {filename}.m4a")

    load_audio(url, f"{filename}.m4a")

def main(directory):
    # get info from Telegraph videos page
    # save URLs we want in a list
    session = HTMLSession()
    response = session.get(telegraph_videos)
    response.html.render(sleep=1, keep_page=True)

    url_list = []
    for url in response.html.absolute_links:
        if "watch?v=" in url:
            url_list.append(url)

    # check if each URL is what we want
    # save audio of so
    for url in url_list:
        log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        if date := check_podcast(url):
            save_podcast(url, date, directory)

    return 0

def usage(msg=None):
    """Print some hopefully useful information."""

    if msg:
        print("*" * 60)
        print(msg)
        print("*" * 60)
    print(__doc__)


########################################################


# parse the CLI params
argv = sys.argv[1:]
try:
    (opts, args) = getopt.getopt(argv, "h", ["help"])
except getopt.GetoptError as err:
    usage(err)
    sys.exit(1)

# parse the options
for (opt, param) in opts:
    if opt in ["-h", "--help"]:
        usage()
        sys.exit(0)

if len(args) != 1:
    usage()
    sys.exit(1)
directory = args[0]

# clear the log file
with open(log_file, "w") as fd:
    pass

# kick it all off
sys.exit(main(directory))
