#
# a small script to empty all *.m4a files.
#

for f in `ls Audio/*.m4a`; do
    echo $f;
    cat /dev/null > $f;
done
