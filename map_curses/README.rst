map_curses
==========

The aim of this bit of code is to extend python curses to allow a scrolling
view of a map drawn in some manner.

This was prompted by 
`this reddit post <https://old.reddit.com/r/learnpython/comments/ow382t/ascii_game_library/>`_
asking for some way of allowing a python terminal program to create something
like the interface of
`the nlsnipes game <https://en.wikipedia.org/wiki/Snipes_(video_game)>`_.
This game has a vaguely curses-like look but python curses can't do the scrolling
view of a large map.

