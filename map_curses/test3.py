import curses
import random

# Please note that curses uses a y,x coordination system.
# In the code you see here, I'll be using x,y

# If any of the code seems inconsistent, it's because I took
# some of the things from my current project out and replaced
# for the purposes of this article

# Also, some of this was poorly converted. If you want to clean
# it up, go for it. Otherwise, I might get around to it eventually ;)

# I have not included placing up/down stairs or any of that business

# This is just the basic dungeon tile. It holds a shape.
class dungeon_tile:
    def __init__(self, shape):
        self.shape = shape

# Our randomly generated dungeon
class dungeon:
    def __init__(self, map_size_x, map_size_y, max_features, room_chance):
        """Initialize the dungeon.

        max_features  how many rooms/corridors generated
        room_chance   the % chance that the generated feature will be a room
        """

        # set the map size
        self.map_size_x = map_size_x
        self.map_size_y = map_size_y

        # start the "tiles" 2D array
        self.tiles = []

        # fill the map with blank tiles
        for x in range (0, self.map_size_x):
            self.tiles.append([dungeon_tile('') for _ in range (0, self.map_size_y)])

        current_features = 1
        self._make_room(self.map_size_x//2, self.map_size_y//2, 5, 5, random.randint(0, 3))

        for i in range (0, 1000):
            if current_features == max_features: break

            newx = 0
            xmod = 0
            newy = 0
            ymod = 0
            direction = -1

            for j in range (0, 1000):
                newx = random.randint(1, self.map_size_x-2)
                newy = random.randint(1, self.map_size_y-2)
                direction = -1

                shape = self.tiles[newx][newy].shape

                if shape == '#' or shape == 'c':
                    if self.tiles[newx][newy+1].shape == '.' or self.tiles[newx][newy+1].shape == 'c':
                        xmod = 0
                        ymod = -1
                        direction = 0
                    elif self.tiles[newx-1][newy].shape == '.' or self.tiles[newx-1][newy].shape == 'c':
                        xmod = 1
                        ymod = 0
                        direction = 1
                    elif self.tiles[newx][newy-1].shape == '.' or self.tiles[newx][newy-1].shape == 'c':
                        xmod = 0
                        ymod = 1
                        direction = 2
                    elif self.tiles[newx+1][newy].shape == '.' or self.tiles[newx+1][newy].shape == 'c':
                        xmod = -1
                        ymod = 0
                        direction = 3

                    if direction > -1: break

            if direction > -1:
                feature = random.randint(0, 100)

                if feature <= room_chance:
                    if self._make_room(newx+xmod, newy+ymod, 10, 10, direction):
                        current_features += 1
                        self.tiles[newx][newy].shape = '+'
                        self.tiles[newx+xmod][newy+ymod].shape = '.'
                elif feature > room_chance:
                    if self._make_corridor(newx+xmod, newy+ymod, 6, direction):
                        current_features += 1
                        self.tiles[newx][newy].shape = '+'

    # this shows the map. we're not doing a scrolling map for this article, so it's easier
    def draw(self, stdscr):
        """Draw map in the window."""

        for x in range (0, self.map_size_x):
            for y in range (0, self.map_size_y):
                stdscr.addstr(y, x, self.tiles[x][y].shape)
        stdscr.refresh()

    # this makes a corridor at x,y in a direction
    def _make_corridor(self, x, y, length, direction):
        len = random.randint(2, length)

        dir = 0
        if direction > 0 and direction < 4: dir = direction

        if dir == 0:
            if x < 0 or x > self.map_size_x-1: return False

            for i in range (y, y-len, -1):
                if i < 0 or i > self.map_size_y-1: return False
                if self.tiles[x][i].shape != '':return False

            for i in range (y, y-len, -1):
                self.tiles[x][i].shape = 'c'
        elif dir == 1:
            if y < 0 or y > self.map_size_y-1: return False

            for i in range (x, x+len):
                if i < 0 or i > self.map_size_y-1: return False
                if self.tiles[i][y].shape != '': return False

            for i in range (x, x+len):
                self.tiles[i][y].shape = 'c'
        elif dir == 2:
            if x < 0 or x > self.map_size_x-1: return False

            for i in range (y, y+len):
                if i < 0 or i > self.map_size_y-1: return False
                if self.tiles[x][i].shape != '': return False

            for i in range (y, y+len):
                self.tiles[x][i].shape = 'c'
        elif dir == 3:
            if y < 0 or y > self.map_size_y-1: return False

            for i in range (x, x-len, -1):
                if i < 0 or i > self.map_size_y-1: return False
                if self.tiles[i][y].shape != '': return False

            for i in range (x, x-len, -1):
                self.tiles[i][y].shape = 'c'

        return True

    # this makes a room at x,y with the width,height and in a direction
    def _make_room(self, x, y, width, height, direction):
        rand_width = random.randint(4, width)
        rand_height = random.randint(4, height)

        dir = 0
        if direction > 0 and direction < 4: dir = direction

        if dir == 0:
            for i in range (y, y-rand_height, -1):
                if i < 0 or i > self.map_size_y-1: return False
                for j in range (x-rand_width//2, (x+(rand_width+1)//2)):
                    if j < 0 or j > self.map_size_x-1: return False
                    if self.tiles[j][i].shape != '': return False

            for i in range (y, y-rand_height, -1):
                for j in range (x-rand_width//2, (x+(rand_width+1)//2)):
                    if j == x-rand_width//2: self.tiles[j][i].shape = '#'
                    elif j == x+(rand_width-1)//2: self.tiles[j][i].shape = '#'
                    elif i == y: self.tiles[j][i].shape = '#'
                    elif i == y-rand_height+1: self.tiles[j][i].shape = '#'
                    else: self.tiles[j][i].shape = '.'
        elif dir == 1:
            for i in range (y-rand_height//2, y+(rand_height+1)//2):
                if i < 0 or i > self.map_size_y-1: return False
                for j in range (x, x+rand_width):
                    if j < 0 or j > self.map_size_x-1: return False
                    if self.tiles[j][i].shape != '': return False

            for i in range (y-rand_height//2, y+(rand_height+1)//2):
                for j in range (x, x+rand_width):
                    if j == x: self.tiles[j][i].shape = '#'
                    elif j == x+(rand_width-1): self.tiles[j][i].shape = '#'
                    elif i == y-rand_height//2: self.tiles[j][i].shape = '#'
                    elif i == y+(rand_height-1)//2: self.tiles[j][i].shape = '#'
                    else: self.tiles[j][i].shape = '.'
        elif dir == 2:
            for i in range (y, y+rand_height):
                if i < 0 or i > self.map_size_y-1: return False
                for j in range (x-rand_width//2, x+(rand_width+1)//2):
                    if j < 0 or j > self.map_size_x-1: return False
                    if self.tiles[j][i].shape != '': return False

            for i in range (y, y+rand_height):
                for j in range (x-rand_width//2, x+(rand_width+1)//2):
                    if j == x-rand_width//2: self.tiles[j][i].shape = '#'
                    elif j == x+(rand_width-1)//2: self.tiles[j][i].shape = '#'
                    elif i == y: self.tiles[j][i].shape = '#'
                    elif i == y+(rand_height-1): self.tiles[j][i].shape = '#'
                    else: self.tiles[j][i].shape = '.'
        elif dir == 3:
            for i in range (y-rand_height//2, y+(rand_height+1)//2):
                if i < 0 or i > self.map_size_y-1: return False
                for j in range (x, x-rand_width, -1):
                    if j < 0 or j > self.map_size_x-1: return False
                    if self.tiles[j][i].shape != '': return False

            for i in range (y-rand_height//2, y+(rand_height+1)//2):
                for j in range (x, x-rand_width-1, -1):
                    if j == x: self.tiles[j][i].shape = '#'
                    elif j == x-rand_width: self.tiles[j][i].shape = '#'
                    elif i == y-rand_height//2: self.tiles[j][i].shape = '#'
                    elif i == y+(rand_height-1)//2: self.tiles[j][i].shape = '#'
                    else: self.tiles[j][i].shape = '.'

        return True

def main(stdscr):
    # get max window size
    screen_y, screen_x = stdscr.getmaxyx()

    # create and draw the dungeon
    this_dungeon = dungeon(screen_x, screen_y, 5, 50)
    this_dungeon.draw(stdscr)

    # wait for keypress
    stdscr.getch()

if __name__ == '__main__':
    curses.wrapper(main)
