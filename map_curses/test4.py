import curses
import random

# Please note that curses uses a y,x coordination system.
# In the code you see here, I'll be using x,y

LogFile = 'test.log'

with open(LogFile, 'w') as fd:
    pass

def log(s):
    with open(LogFile, 'a') as fd:
        fd.write(s + '\n')

# This is just the basic dungeon tile. It holds a shape.
class dungeon_tile:
    def __init__(self, shape):
        self.shape = shape

# Our randomly generated dungeon
class dungeon:
    def __init__(self, map_size_x, map_size_y, view_size_x, view_size_y):
        """Initialize the dungeon.

        map_size_x      actual map size, X and Y
        map_size_y
        view_size_x     view window size, X and Y
        view_size_y
        """

        # set the map and view sizes
        self.map_size_x = map_size_x
        self.map_size_y = map_size_y

        self.view_size_x = view_size_x
        self.view_size_y = view_size_y

        log(f'dungeon: .map_size_x={self.map_size_x}, .map_size_y={self.map_size_y}')
        log(f'dungeon: .view_size_x={self.view_size_x}, .view_size_y={self.view_size_y}')

        # set view offset from map
        self.offset_x = 0
        self.offset_y = 0

        # start the "tiles" 2D array
        self.tiles = []

        # fill the map with blank tiles
        for x in range (0, self.map_size_x):
            self.tiles.append([dungeon_tile('') for _ in range (0, self.map_size_y)])

        # put in "tick" marks every 10 tiles
        for x in range (0, self.map_size_x, 10):
            col = (x // 10) % 10
            for y in range (0, self.map_size_y, 10):
                row = (y // 10) % 10
                self.tiles[x][y].shape = '.'
                if x+1 < self.map_size_x - 1:
                    self.tiles[x+1][y].shape = str(col)
                if y+1 < self.map_size_y - 1:
                    self.tiles[x][y+1].shape = str(row)

    def scroll_up(self):
        log(f'scroll_up: before, .offset_y={self.offset_y}')
        self.offset_y -= 1
        if self.offset_y < 0:
            self.offset_y = 0
        log(f'scroll_up: after, .offset_x={self.offset_x}, .offset_y={self.offset_y}')

    def scroll_down(self):
        log(f'scroll_down: before, .offset_y={self.offset_y}, self.map_size_y - self.view_size_y={self.map_size_y - self.view_size_y}')
        self.offset_y += 1
        if self.offset_y > (self.map_size_y - self.view_size_y):
            self.offset_y = self.map_size_y - self.view_size_y
        log(f'scroll_down: after, .offset_x={self.offset_x}, .offset_y={self.offset_y}')

    def scroll_left(self):
        log(f'scroll_left: before, .offset_x={self.offset_x}')
        self.offset_x -= 1
        if self.offset_x < 0:
            self.offset_x = 0
        log(f'scroll_left: after, .offset_x={self.offset_x}, .offset_y={self.offset_y}')

    def scroll_right(self):
        log(f'scroll_right: before, .offset_x={self.offset_x}')
        self.offset_x += 1
        if self.offset_x > (self.map_size_x - self.view_size_x):
            self.offset_x = self.map_size_x - self.view_size_x
        log(f'scroll_right: after, .offset_x={self.offset_x}, .offset_y={self.offset_y}')

    def draw(self, stdscr):
        """Draw map in the window.

        Draw a view of the map, starting at the X and Y offsets.
        """

        log(f'draw: .offset_x={self.offset_x}, .offset_y={self.offset_y}')
        stdscr.erase()
        for x in range (0, self.view_size_x):
            for y in range (0, self.view_size_y):
                stdscr.addstr(y, x, self.tiles[x+self.offset_x][y+self.offset_y].shape)
        stdscr.refresh()

def main(stdscr):
    # get max window size
    screen_y, screen_x = stdscr.getmaxyx()

    # create and draw the dungeon
    this_dungeon = dungeon(998, 998, screen_x, screen_y)
    this_dungeon.draw(stdscr)

    # get and check keypresses
    while True:
        ch = stdscr.getch()
        if ch == 259:   # UP
            this_dungeon.scroll_up()
        elif ch == 258: # DOWN
            this_dungeon.scroll_down()
        elif ch == 261: # RIGHT
            this_dungeon.scroll_right()
        elif ch == 260: # LEFT
            this_dungeon.scroll_left()
        elif ch == ord('q'):
            break
        this_dungeon.draw(stdscr)

if __name__ == '__main__':
    curses.wrapper(main)
