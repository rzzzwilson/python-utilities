bucheck
=======

The program *backup* is used to backup source directories to target
directories.  It uses rsync to back up only changed files to the target.
The backup occurs into a directory with a timestamp name.  The rsync option
that allows a hard link to a previous backup directory if the file to be backed
up hasn't changed is used.

The *bucheck* program will take two of these complete backup directories and
report on changes to files in the backups.

Usage
=====

The *bucheck* program is used:

    Usage: bucheck <options> <dir1> <dir2>

    where <dir1> and <dir2> are the two backup directories to compare,
    and <options> may be zero or more of:
        -h  --help  print help and then stop
        -a  --all   print all files
                    (the default is to print only changed files)

The output produced by the program will look like:

     2015_02_10                  | 2015_03_21
    -----------------------------+-----------------------------
     file1.txt                   !
     file2.txt                   = file2.txt
     file3.txt                   x file3.txt
                                 ! file4.txt
     file5/                      ! 
                                 ! file6/

In the above, *file1.txt* exists only in the *2015_02_10* backup.  Similarly,
the file *file4.txt* exists only in the *2015_03_21* backup.  The directories
*file5* and *file6* also exist in only one backup directory.

The file *file2.txt* exists unchanged in both backups, but file *file3.txt*
is different between the backups.  The *x* central delimiter shows the file is
different between backups.

