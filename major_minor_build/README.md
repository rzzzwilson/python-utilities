# major_minor_build

This isn't really a python utility, but I had trouble deciding where
it should live and, since it uses python for the system tools I finally
put it here.

The system described here is a way to semi-automatically manage the 
major/minor/build version numbers of a software package.

## What the numbers mean

Most systems have a method of numbering releases that is something like:

    3.12.4

which is the version of the latest python release at the time of writing.
The first number `3` is the **major** release number.  The second (`12`)
is the **minor** release number.  The last number, which I call the **build**
number can be a bugfix number or a build number, etc.

The point of those numbers is to give a unique version numer to released software.
The usual approach is to increase the build number by one for every build of
the software (or every bugfix release).  The minor number increases by one
with every release that is different enough from the previous release, and the 
major number increases by one for every major release which contains changes
that might even be backward incompatible.

It is usual that an increment of the major or minor numbers forces a reset
of all numbers to the right of the incremented number.  For example after
a release of the 3.12.4 version of some software a major change would lead
to the next release having a version of 4.0.0.

## How to make this less painful

When changing version numbers it's possible, of course, to just find all 
places in the source code where the version numbers are stored and change
the numbers to the new version.  That's painful and error-prone, so any
sensible programmer would have the version numbers stored in one place and any 
software that needed to display the numbers would get them from that single
place.

But changing numbers is still something a human has to do, so it's possible
mistakes will be made.  How can we automate it even more?

One approach is to use your software build system.  Here I use `make`, but 
any modern build system should be able to do something similar.

## User interface

The `make` command can take a `target` as a parameter.  A `make` build system
usually has a `clear` target that can be used to remove unwanted files and 
directories by the user typing `make clear`.

We make special targets `major`, `minor` and `build`.  Each of those targets
increments the appropriate number of the version numbers and handles resetting
other numbers.  The `build` target would just be the standard `make` command
used to build the software if the final version number is an actual build
number.

The version number information would be held in a single file, of course.  Any 
python or C code, for example, would just `import` or `#include` that file to
access the version numbers.  So it makes sense that the version numbers would
be held in that file in a form that makes it easy for the software to access
the numbers.  A python version file, perhapse called `version.py` might
contain this:

    """
    Version numbers for the Xyzzy software package
    """

    major = 3
    minor = 12
    build = 4

and the code to use that might be:

    import version

    print(f"Xyzzy {version.major}.{version.minor}.{version.build}")

So if the user enters the command `make minor` the `minor` target in the 
Makefile is executed.  That will execute a program that reads in the `version.py`
file, increments the minor number and resets the build number, before continuing
on with a full or partial make.  A sample of a simple *Makefile* is:

    clean:
    	rm -Rf old_version.py
    
    major:
    	cp version.py old_version.py
    	python mmb.py version.py major
    	# would do "make clean" and then a full build
    	# but for testing, just show results
    	cat version.py
    	mv old_version.py version.py
    
    minor:
    	cp version.py old_version.py
    	python mmb.py version.py minor
    	# would do "make clean" and then a full build
    	# but for testing, just show results
    	cat version.py
    	mv old_version.py version.py
    
    build:
    	cp version.py old_version.py
    	python mmb.py version.py build
    	# would do "make clean" and then a partial build
    	# but for testing, just show results
    	cat version.py
    	mv old_version.py version.py

