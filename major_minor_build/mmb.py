"""
A program to read a python "version" file, increment the required
major/minor/build numbers as required and reset any required numbers.
The updated "version" information is written back to the given file.

Usage:  mmb <version_file> [major|minor|build]
or:     mmb --reset <version_file> <major> <minor> <build>

where <version_file>   is the version file to modify
and   major          | 
      minor          + are the numbers to increment
      build          |

The --reset version resets the data in <version_file> to the
given <major>, <minor> and <build> numbers.
"""

import sys


def mmb(filename, vernum):
    """Increment the "vernum" version number in file "filename".

    Returns 0 if there was no error, else returns a non-zero error number.
    """

    # read data from the given file
    try:
        with open(filename) as fd:
            lines = fd.readlines()
    except FileNotFoundError:
        print(f"Sorry, filename '{filename}' doesn't exist.")
        return 2

    # set the version numbers to default values
    major = None
    minor = None
    build = None

    # set error flag to "no error"
    error = False

    # parse out the version numbers, passing unrecognized
    # lines through, creating a new list of updated lines
    # minus the version number lines
    new_lines = []
    for (linenum, line) in enumerate(lines):
        line = line.rstrip()
        split_line = line.split("=")
        if len(split_line) != 2:
            # something else, pass through unchanged
            new_lines.append(line)
            continue

        (name, value) = split_line
        name = name.strip().lower()
        value = value.strip()
        try:
            value = int(value)
        except ValueError:
            # something else, pass through unchanged
            new_lines.append(line)
            continue

        # update the version numbers
        if name in {"major", "minor", "build"}:
            # a version number line
            if name == "major":
                if major is not None:
                    # seen "major" twice?
                    print("Sorry, 'major' number repeated on line {linenum+1}")
                    error = True
                major = value
            elif name == "minor":
                if minor is not None:
                    # seen "minor" twice?
                    print("Sorry, 'minor' number repeated on line {linenum+1}")
                    error = True
                minor = value
            else:
                # must be "build"
                if build is not None:
                    # seen "build" twice?
                    print("Sorry, 'build' number repeated on line {linenum+1}")
                    error = True
                build = value
        else:
            # something else, just pass through unchanged
            new_lines.append(line)

    # make sure we have all three version numbers
    if not all((major, minor, build)):
        error = True
        print(f"Version numbers are missing in file '{filename}':")
        if not major:
            print("    major")
        if not minor:
            print("    minor")
        if not build:
            print("    build")

    # if errors, quit now
    if error:
        return 3

    # update the appropriate version numbers
    if vernum == "major":
        major += 1
        minor = 0
        build = 0
    elif vernum == "minor":
        minor += 1
        build = 0
    elif vernum == "build":
        build += 1
    else:
        print(f"Bad version number name: {vernum}")
        return 4

    # write the changed data back to the file
    with open(filename, "w") as fd:
        fd.write("\n".join(new_lines))
        fd.write("\n")
        fd.write(f"major = {major}\n")
        fd.write(f"minor = {minor}\n")
        fd.write(f"build = {build}\n")

    # finished, return "no error"
    return 0

def mmb_reset(filename, major, minor, build):
    """Reset data in "filename" back to the given major/minor/build numbers."""

    # read data from the given file
    try:
        with open(filename) as fd:
            lines = fd.readlines()
    except FileNotFoundError:
        print(f"Sorry, filename '{filename}' doesn't exist.")
        return 2

    # parse out the version numbers, passing unrecognized
    # lines through, creating a new list of updated lines
    # minus the version number lines
    new_lines = []
    for (linenum, line) in enumerate(lines):
        line = line.rstrip()
        split_line = line.split("=")
        if len(split_line) != 2:
            # something else, pass through unchanged
            new_lines.append(line)
            continue

        (name, value) = split_line
        name = name.strip().lower()
        value = value.strip()
        try:
            value = int(value)
        except ValueError:
            # something else, pass through unchanged
            new_lines.append(line)
            continue

    # now write a new data file, adding new major/minor/build
    # lines at the end of the file
    with open(filename, "w") as fd:
        fd.write("\n".join(new_lines))
        fd.write("\n")
        fd.write(f"major = {major}\n")
        fd.write(f"minor = {minor}\n")
        fd.write(f"build = {build}\n")

    # finished, return "no error"
    return 0

################################################################

def usage(msg=None):
    """Print some help and optional message."""

    if msg:
        print(f"{'*' * 80}")
        print(msg)
        print(f"{'*' * 80}")
        print(__doc__)


result = 10     # assume an error

if len(sys.argv) == 3:
    # normal usage
    (_, filename, vernum) = sys.argv
    result = mmb(filename, vernum)
elif len(sys.argv) == 6:
    # possible reset
    if sys.argv[1] in {"--reset", "-r"}:
        result = mmb_reset(*sys.argv[2:])
    else:
        usage()
else:
    usage()

sys.exit(result)
