"""
Test program to show usage of "version.py".

Use with the Makefile build system.
"""

import version


print(f"main.py: {version.major}.{version.minor}.{version.build}")
