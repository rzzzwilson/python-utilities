"""
Version numbers for the Xyzzy software package.
"""

major = 3
minor = 12
build = 4
